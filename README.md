# USSRM

USSRM, or the United Socialist Repositories of Minecraft, is a
[Minecraft](https://minecraft.net) server implementation written entirely
in [Kotlin](https://kotlin-lang.org).

Want to get involved? Send us a few merge requests and/or join
our [Discord guild](https://discord.gg/P2qjYmP)!

### Downloads

The project is currently not in a usable state to be released.

### Development

In order to start developing, you'll need to first set the Kotlin plugin
to use the 1.3 release candidate:

![https://uwu.whats-th.is/268d54.png](https://uwu.whats-th.is/268d54.png)

After that, simply run `git clone git@gitlab.com:USSRM/kotlin.git`,
open the project and get hammering at the code!