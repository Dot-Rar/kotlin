/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.world

import com.proximyst.ussrm.api.data.UniquelyIdentifiable
import java.util.*

/**
 * A world within the game, holding data about the environment and anything else found in it.
 *
 * It is possible to be identified using a [UUID], as it extends [UniquelyIdentifiable].
 */
interface World : UniquelyIdentifiable {
    /**
     * The name of the world. This is a variable as it can be changed, and shouldn't be identified using it.
     */
    var name: String

    /**
     * The name of the directory meant for storing data about this world.
     *
     * This is dependent on the original world name, and cannot be changed past this state.
     *
     * It is in theory safe to identify by this, though it is far from recommended, seeing as it may
     * still be possible to just change the directory name manually, in addition to how it is most likely using
     * more bytes to store it than the UUID's 8 bytes.
     */
    val dataDirectoryName: String
}