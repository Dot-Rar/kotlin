/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.network

import java.util.*

/**
 * A game profile contains the data which the server needs about a user, as fetched from Mojang.
 *
 * All data here has been verified by Mojang and is valid to use.
 */
data class GameProfile(
    /**
     * The UUID of the player this stores data on.
     */
    val uuid: UUID,

    /**
     * The username of the player this stores data on.
     *
     * Be noted that in the earliest of stages, this can be wrong in CaSeS, but will not be that later on.
     */
    val username: String,

    /**
     * Contains all properties we have to store about the player.
     *
     * The most used property is the skin, but it is not meant for only that.
     */
    val properties: MutableMap<String, Property> = mutableMapOf()
) {
    /**
     * Contains data about a property as stored in [properties].
     */
    data class Property(
        /**
         * The name of the property stored.
         */
        val name: String,

        /**
         * The value of the property stored.
         *
         * It may be a single integer or other forms of properties, but it will always be a [String] here, no matter
         * what the string contains.
         */
        val value: String,

        /**
         * The signature of this property, which can then be used to verify the legibility of the property.
         */
        val signature: String
    )
}