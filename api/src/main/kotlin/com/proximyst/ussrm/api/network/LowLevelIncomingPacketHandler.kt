/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.network

import com.proximyst.ussrm.packet.lowlevel.Packet
import io.netty.channel.ChannelHandlerContext

/**
 * Handles incoming low-level packets of type [T].
 */
interface LowLevelIncomingPacketHandler<T>
        where T : Packet {
    /**
     * Handles the [packet] given.
     *
     * @param packet
     *    The packet to handle; this always has the type of [T].
     *
     * @param channelContext
     *    The channel context to use.
     *
     * @param connection
     *    The connection of the player.
     */
    fun handle(packet: T, channelContext: ChannelHandlerContext, connection: PlayerConnection)
}