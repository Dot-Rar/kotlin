/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.network

import com.proximyst.ussrm.packet.lowlevel.Packet
import io.netty.channel.ChannelHandlerContext

/**
 * The low-level packet manager to use for handling packets.
 */
interface LowLevelPacketManager {
    /**
     * Listen to the [Packet] of [T] using [handler].
     *
     * @param type
     *    The [Packet] type to use.
     *
     * @param handler
     *    The [LowLevelIncomingPacketHandler] to use when handling the packets of type [T].
     */
    fun <T : Packet> listen(type: Class<T>, handler: LowLevelIncomingPacketHandler<out T>)

    /**
     * Handle the [packet] of type [T] using the handlers registered through [listen].
     *
     * @param packet
     *    The [Packet] of [T] to handle.
     *
     * @param channelContext
     *    The channel context.
     *
     * @param connection
     *    The connection of the player.
     */
    fun <T : Packet> handle(packet: T, channelContext: ChannelHandlerContext, connection: PlayerConnection)
}