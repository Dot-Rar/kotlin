/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.world

/**
 * Stores data about a location without a world tied to it.
 * This is useful for mathematical stuff or locations which are world unspecific.
 *
 * This is also how the [Location]'s data is stored.
 */
data class Vector3D(
    /**
     * The X coordinate of the Vector.
     */
    var x: Double = 0.0,

    /**
     * The Y coordinate of the Vector.
     */
    var y: Double = 0.0,

    /**
     * The Z coordinate of the Vector.
     */
    var z: Double = 0.0,

    /**
     * The pitch of the Vector.
     */
    var pitch: Float = 0f,

    /**
     * The yaw of the Vector.
     */
    var yaw: Float = 0f
)