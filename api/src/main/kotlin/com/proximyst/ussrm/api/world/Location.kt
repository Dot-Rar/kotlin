/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.world

/**
 * A [Vector3D] linked to a world. This simply stores a position and the world it is in.
 */
data class Location(
    /**
     * The position of the location: where in the [world] it is.
     */
    var vector: Vector3D = Vector3D(),

    /**
     * The world the location is located in.
     */
    var world: World
) {
    @JvmOverloads
    constructor(
        x: Double = 0.0,
        y: Double = 0.0,
        z: Double = 0.0,
        pitch: Float = 0.0f,
        yaw: Float = 0.0f,
        world: World
    ) : this(Vector3D(x, y, z, pitch, yaw), world)

    var x: Double
        get() = vector.x
        set(value) {
            vector.x = value
        }

    var y: Double
        get() = vector.y
        set(value) {
            vector.y = value
        }

    var z: Double
        get() = vector.z
        set(value) {
            vector.z = value
        }

    var pitch: Float
        get() = vector.pitch
        set(value) {
            vector.pitch = value
        }

    var yaw: Float
        get() = vector.yaw
        set(value) {
            vector.yaw = value
        }
}