/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.data

import io.netty.buffer.ByteBuf

/**
 * Returns a ByteArray of the entire buffer safely.
 *
 * This should never throw an exception, unlike [ByteBuf.array].
 */
fun ByteBuf.safeArray(): ByteArray {
    if (hasArray()) return array()

    markReaderIndex()
    val buffer = ByteArray(readableBytes())
    readBytes(buffer)
    resetReaderIndex()
    return buffer
}