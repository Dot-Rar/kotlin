/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.network

import com.proximyst.ussrm.packet.lowlevel.Packet
import io.netty.channel.ChannelHandlerContext

/**
 * Listens to the given [Packet] of type [T] using [handler].
 *
 * @param T
 *    The type of [Packet].
 *
 * @param handler
 *    The handler to use to listen to [T].
 */
inline fun <reified T : Packet> LowLevelPacketManager.listen(handler: LowLevelIncomingPacketHandler<T>) {
    listen(T::class.java, handler)
}

/**
 * Listens to the given [Packet] of type [T] using [handler].
 *
 * @param T
 *    The type of [Packet].
 *
 * @param handler
 *    The handler to use to listen to [T].
 */
inline fun <reified T : Packet> LowLevelPacketManager.listen(crossinline handler: (T, ChannelHandlerContext, PlayerConnection) -> Unit) {
    listen(object : LowLevelIncomingPacketHandler<T> {
        override fun handle(
            packet: T,
            channelContext: ChannelHandlerContext,
            connection: PlayerConnection
        ) = handler(packet, channelContext, connection)
    })
}