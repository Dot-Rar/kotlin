/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.api.network

import com.proximyst.ussrm.api.Server
import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import io.netty.channel.socket.SocketChannel
import java.net.InetSocketAddress
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey

/**
 * A connection to the player which they initiated.
 *
 * This is used to send packets and such over the wire, and it can be used for lower level changes and settings,
 * if the need arises. This is not recommended to use unless absolutely necessary.
 */
class PlayerConnection(
    /**
     * The socket of the connection to the player.
     *
     * This represents a direct connection with the player's client.
     */
    val channel: SocketChannel,

    /**
     * The address of the player. There is no standard as to whether this is IPv4 or IPv6.
     */
    val address: InetSocketAddress
) {
    /**
     * The state the player owning this connection currently is in.
     */
    var state = Packet.State.HANDSHAKE

    /**
     * The protocol version of the player.
     *
     * On the standard server, this will be equal to the protocol version of [Server.protocolVersion].
     * This can be useful for plugins which are along the lines of Spigot's ViaVersion, in order to support
     * several Minecraft versions.
     */
    var protocolVersion = -1

    /**
     * The port the player connected to with their client. This is almost always safe to assume is 25565.
     *
     * The Notchian server does not use this.
     */
    var connectingPort: UShort = 25565u

    /**
     * The hostname of the server which the player used to connect to the server.
     *
     * The Notchian server does not use this.
     */
    var connectingHost: String? = null

    /**
     * The settings for compression for this specific connection.
     *
     * If this is modified, remember to handle the [SocketChannel.pipeline] of [channel].
     */
    var compressionSettings = CompressionSettings()

    /**
     * The settings for encryption for this specific connection.
     *
     * If this is modified, remember to handle the [SocketChannel.pipeline] of [channel].
     */
    var encryptionSettings = EncryptionSettings()

    /**
     * The username of the player this connection belongs to.
     *
     * This is only known to be safe after [state] enters [Packet.State.PLAY].
     */
    lateinit var username: String

    /**
     * The UUID of the player this connection belongs to.
     *
     * This is only known to be safe after [state] enters [Packet.State.PLAY].
     */
    lateinit var uuid: UUID

    /**
     * The game profile of the player this connection belongs to.
     *
     * This is only known to be safe after the [state] enters [Packet.State.PLAY].
     */
    lateinit var gameProfile: GameProfile

    /**
     * Writes a packet to the [channel]'s pipeline, making it handle the data given.
     *
     * This does not ignore the pipeline, and will encode, encrypt, and so on.
     *
     * @param T
     *      The type of the packet to send. This is a hack around using contracts and another interface.
     *
     * @param packet
     *      The packet to write and send to the player.
     */
    fun <T> writePacket(packet: T)
            where T : Packet,
                  T : Encodable {
        channel.pipeline().writeAndFlush(packet)
    }

    /**
     * Settings for compression for players.
     *
     * This should never be modified directly from plugins, though it is allowed.
     */
    data class CompressionSettings(
        /**
         * Whether compression is enabled or not.
         *
         * When this is changed, remember to send the compression packet too.
         */
        var enabled: Boolean = false,

        /**
         * The threshold of the packet size before it has to be compressed.
         *
         * A negative value indicates this is disabled, and [enabled] should reflect that.
         *
         * When this is changed, remember to send the compression packet too.
         */
        var threshold: Int = -1
    )

    /**
     * Settings for encryption for players.
     *
     * This should never be modified directly from plugins, though it is allowed.
     */
    data class EncryptionSettings(
        /**
         * Whether encryption is enabled or not.
         *
         * When this is changed, remember to send the encryption packet too.
         */
        var enabled: Boolean = false,

        /**
         * The shared symmetrical key used for encryption and decryption on both ends.
         *
         * This should only be changed upon changing [enabled].
         *
         * When this is changed, remember ot send the encryption packet too.
         */
        var sharedKey: SecretKey? = null,

        /**
         * The cipher to use for encrypting packets to players.
         *
         * This should only be changed upon changing the [sharedKey] or [enabled].
         */
        var encryptionCipher: Cipher? = null,

        /**
         * The cipher to use for decrypting packets from players.
         *
         * This should only be changed upon changing the [sharedKey] or [enabled].
         */
        var decryptionCipher: Cipher? = null
    )
}