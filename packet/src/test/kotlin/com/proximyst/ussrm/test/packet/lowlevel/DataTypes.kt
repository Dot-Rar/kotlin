/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.test.packet.lowlevel

import com.proximyst.ussrm.packet.lowlevel.data.*
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.netty.buffer.Unpooled

class DataTypes : StringSpec() {
    init {
        "angles" {
            for (iter in UByte.MIN_VALUE.toInt() until UByte.MAX_VALUE.toInt()) {
                val from = iter.toUByte()
                val angle = from.toAngle()
                val encoded = angle.encode()
                val decoded = PacketAngle.decode(encoded)
                decoded?.contained() shouldBe from
                val array = ByteArray(1)
                encoded.resetReaderIndex()
                encoded.readBytes(array)
                array shouldBe byteArrayOf(from.toByte())
            }
        }

        "booleans" {
            "boolean: true" {
                val bool = true.toPacket()
                val encoded = bool.encode()

                "boolean encode true" {
                    val array = ByteArray(1)
                    encoded.resetReaderIndex()
                    encoded.readBytes(array)
                    array shouldBe byteArrayOf(0x01)
                }

                "boolean decode true" {
                    val decoded = PacketBoolean.decode(encoded)
                    decoded?.contained() shouldBe true
                }
            }

            "boolean: false" {
                val bool = false.toPacket()
                val encoded = bool.encode()

                "boolean encode false" {
                    val array = ByteArray(1)
                    encoded.resetReaderIndex()
                    encoded.readBytes(array)
                    array shouldBe byteArrayOf(0x00)
                }

                "boolean decode true" {
                    val decoded = PacketBoolean.decode(encoded)
                    decoded?.contained() shouldBe false
                }
            }

            "boolean decode 0x02 (false)" {
                val decoded = PacketBoolean.decode(Unpooled.copiedBuffer(byteArrayOf(0x02)))
                decoded?.contained() shouldBe false
            }
        }

        "bytes" {
            for (iter in Byte.MIN_VALUE.toInt() until Byte.MAX_VALUE.toInt()) {
                val from = iter.toByte()
                val packet = from.toPacket()
                val encoded = packet.encode()
                val decoded = PacketByte.decode(encoded)
                decoded?.contained() shouldBe from
                val array = ByteArray(1)
                encoded.resetReaderIndex()
                encoded.readBytes(array)
                array shouldBe byteArrayOf(from)
            }
        }

        "varints" {
            for ((key, value) in mapOf(
                0 to byteArrayOf(0x00.toByte()),
                1 to byteArrayOf(0x01.toByte()),
                2 to byteArrayOf(0x02.toByte()),
                127 to byteArrayOf(0x7f.toByte()),
                128 to byteArrayOf(0x80.toByte(), 0x01.toByte()),
                255 to byteArrayOf(0xFF.toByte(), 0x01.toByte()),
                2147483647 to byteArrayOf(0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0x07.toByte()),
                -1 to byteArrayOf(0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0x0F.toByte()),
                -2147483648 to byteArrayOf(0x80.toByte(), 0x80.toByte(), 0x80.toByte(), 0x80.toByte(), 0x08.toByte())
            )) {
                "varint encode and decode of $key" {
                    val varInt = key.toVarInt()
                    val encoded = varInt.encode()
                    val decoded = VarInt.decode(encoded)
                    decoded?.contained() shouldBe key
                    val array = ByteArray(value.size)
                    encoded.resetReaderIndex()
                    encoded.readBytes(array)
                    array shouldBe value
                }
            }

            "invalid varint null" {
                val invalid = byteArrayOf(
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0x7F.toByte()
                ) // varlong 9223372036854775807
                VarInt.decode(Unpooled.copiedBuffer(invalid)) shouldBe null
            }
        }

        "varlongs" {
            "varlong encode and decode of 9223372036854775807" {
                val from = 9223372036854775807L
                val varLong = from.toVarLong()
                val encoded = varLong.encode()
                val decoded = VarLong.decode(encoded)
                decoded?.contained() shouldBe from
                val array = ByteArray(9)
                encoded.resetReaderIndex()
                encoded.readBytes(array)
                array shouldBe byteArrayOf(
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0x7F.toByte()
                )
            }

            "invalid varlong null" {
                val invalid = byteArrayOf(
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0xFF.toByte(),
                    0x7F.toByte()
                ) // invalid varlong; no value
                VarInt.decode(Unpooled.copiedBuffer(invalid)) shouldBe null
            }
        }
    }
}