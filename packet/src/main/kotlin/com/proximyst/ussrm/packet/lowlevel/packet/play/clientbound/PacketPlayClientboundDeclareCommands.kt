/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import com.proximyst.ussrm.packet.lowlevel.data.play.Node
import io.netty.buffer.ByteBuf

/**
 * Lists all of the commands on the server, and how they are parsed.
 */
class PacketPlayClientboundDeclareCommands(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * Number of elements in the nodes array
     */
    val count: VarInt by data

    /**
     * An array of command nodes
     * @see com.proximyst.ussrm.packet.lowlevel.data.play.Node
     */
    val nodes: MutableCollection<Node> by data

    /**
     * Index of the root node in the nodes array
     */
    val rootIndex: VarInt by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        TODO("Implement properties type for parser in Node")

        /*val buf = count.encode(options)

        nodes.forEach { node ->
            buf.writeBytes(node.flags.toPacket().encode(options))
                .writeBytes(node.childrenCount.encode(options))

            node.children.forEach { child -> buf.writeBytes(child.encode(options)) }

            node.redirectNode?.let { redirectNode -> buf.writeBytes(redirectNode.encode(options)) }
            node.name?.let { name -> buf.writeBytes(name.toPacket().encode(options)) }
            node.parser?.let { parser -> buf.writeBytes(parser.toPacket().encode(options)) }
            node.properties?.let { properties -> buf.writeBytes(properties.toPacket().encode(options)) }
            node.suggestionsType?.let { suggestionsType -> buf.writeBytes(suggestionsType.toPacket().encode(options)) }
        }

        buf.writeBytes(rootIndex.encode(options))

        return buf*/
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x11
        override val state = Packet.State.PLAY
    }
}