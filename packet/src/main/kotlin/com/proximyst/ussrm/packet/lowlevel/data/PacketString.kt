/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.data

import com.proximyst.ussrm.packet.lowlevel.Decodable
import io.netty.buffer.ByteBuf

class PacketString(private val value: String) : DataType<PacketString, String>, Decodable<PacketString> by Companion {
    override fun encode(options: Map<String, Any>?): ByteBuf {
        if (value.length > 32767)
            throw IllegalStateException("string is too long")
        val buf = VarInt(value.length).encode()
        buf.writeBytes(value.toByteArray())
        return buf
    }

    override fun contained() = value

    companion object : Decodable<PacketString> {
        override fun decode(data: ByteBuf): PacketString? {
            val length = VarInt.decode(data) ?: return null
            return PacketString(data.readCharSequence(length.contained(), Charsets.UTF_8).toString())
        }
    }
}

fun String.toPacket() = PacketString(this)