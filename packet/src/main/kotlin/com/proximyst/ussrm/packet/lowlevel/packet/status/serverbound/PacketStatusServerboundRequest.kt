/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.status.serverbound

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import io.netty.buffer.ByteBuf

/**
 * Requests the server list data from the server.
 */
class PacketStatusServerboundRequest(
    override val data: MutableMap<String, Any?> = mutableMapOf()
) : Packet, Decodable<PacketStatusServerboundRequest> by Companion, PacketData by Companion {
    companion object : PacketData, Decodable<PacketStatusServerboundRequest> {
        override val bind = Packet.Bind.SERVER
        override val id = 0x00
        override val state = Packet.State.STATUS

        override fun decode(data: ByteBuf): PacketStatusServerboundRequest? {
            return PacketStatusServerboundRequest()
        }
    }
}