/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.handshake.serverbound

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.PacketString
import com.proximyst.ussrm.packet.lowlevel.data.PacketUShort
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import io.netty.buffer.ByteBuf

/**
 * Handshaking, starting the connection to the server.
 * This should never be encrypted nor compressed.
 */
class PacketHandshakeServerboundHandshake(
    override val data: MutableMap<String, Any?>
) : Packet, Decodable<PacketHandshakeServerboundHandshake> by Companion, PacketData by Companion {
    /**
     * The protocol version the client is using.
     * This can be used by plugins to determine whether the client should be treated differently.
     */
    val protocolVersion: Int by data

    /**
     * The server address the client used to connect.
     * Plugins can use this for different stuff, however the Notchian server doesn't use this.
     */
    val serverAddress: String by data

    /**
     * Internal for the public [serverPort].
     *
     * @see [serverPort]
     */
    private val _serverPort: Int by data

    /**
     * The port the client used to connect.
     * The Notchian server doesn't use this, and it has few cases where it's useful.
     */
    val serverPort: UShort
        get() = _serverPort.toUShort()

    /**
     * The next state to be put into for the user.
     * This determines what to send back and what to expect further on.
     *
     * The only possible values are [Packet.State.STATUS] and [Packet.State.LOGIN].
     */
    val nextState: Packet.State by data

    companion object : PacketData, Decodable<PacketHandshakeServerboundHandshake> {
        override val bind = Packet.Bind.SERVER
        override val id = 0x00
        override val state = Packet.State.HANDSHAKE

        override fun decode(data: ByteBuf): PacketHandshakeServerboundHandshake? {
            val protocolVersion = VarInt.decode(data) ?: return null
            val serverAddress = PacketString.decode(data) ?: return null
            val serverPort = PacketUShort.decode(data) ?: return null
            val nextState = VarInt.decode(data) ?: return null

            return PacketHandshakeServerboundHandshake(
                mutableMapOf(
                    "protocolVersion" to protocolVersion.contained(),
                    "serverAddress" to serverAddress.contained(),
                    "_serverPort" to serverPort.contained().toInt(),
                    "nextState" to when (nextState.contained()) {
                        1 -> Packet.State.STATUS
                        2 -> Packet.State.LOGIN
                        else -> return null
                    }
                )
            )
        }
    }
}