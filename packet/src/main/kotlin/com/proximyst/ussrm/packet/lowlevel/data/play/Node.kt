/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.data.play

import com.proximyst.ussrm.packet.lowlevel.data.VarInt

/**
 * **For more information, see:**
 * <https://wiki.vg/Command_Data>
 */
data class Node(
    /**
     * Information about the command
     * - 0x03: Node type (0: root, 1: literal, 2: argument, 3: unused)
     * - 0x04: Is executable (True if the node is a valid command)
     * - 0x08: Has redirect (True if the node redirects to another command (alias))
     * - 0x10: Has suggestions type (Only present for argument nodes)
     */
    val flags: Byte,

    /**
     * Number of elements in the children array
     */
    val childrenCount: VarInt,

    /**
     * Array of indices of child nodes
     */
    val children: MutableCollection<VarInt>,

    /**
     * Present if the flag is 0x08. Index of the redirect node
     */
    val redirectNode: VarInt?,

    /**
     * Name of the command. Present for only argument and literal nodes
     */
    val name: String?,

    /**
     * Identifier for the parser. See <https://wiki.vg/Command_Data#Parsers>
     */
    val parser: String?,

    /**
     * Properties of the parser. See <https://wiki.vg/Command_Data#Properties>
     */
    val properties: Nothing? = TODO(),

    /**
     * Present if the flag is 0x10. See <https://wiki.vg/Command_Data#Suggestions_Types>
     */
    val suggestionsType: String?
)
