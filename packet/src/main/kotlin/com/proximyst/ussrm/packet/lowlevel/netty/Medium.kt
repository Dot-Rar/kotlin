/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.netty

class Medium(internal val value: Int) : Comparable<Medium> {
    companion object {
        val MIN_VALUE: Medium = Medium(-8_388_608)
        val MAX_VALUE: Medium = Medium(8_388_607)
        const val SIZE_BYTES: Int = 3
        const val SIZE_BITS: Int = 24
    }

    operator fun compareTo(other: Byte): Int = value.compareTo(other)
    operator fun compareTo(other: Short): Int = value.compareTo(other)
    operator fun compareTo(other: Long): Int = value.compareTo(other)
    operator fun compareTo(other: Float): Int = value.compareTo(other)
    operator fun compareTo(other: Double): Int = value.compareTo(other)
    operator fun compareTo(other: Char): Int = value.compareTo(other.toShort())
    operator fun compareTo(other: Int): Int = value.compareTo(other)
    operator fun compareTo(other: UInt): Int = value.compareTo(other.toInt())
    operator fun compareTo(other: Number): Int = value.compareTo(other.toDouble())
    override operator fun compareTo(other: Medium): Int = value.compareTo(other.value)

    operator fun plus(other: Byte): Medium = this.plus(other.toMedium())
    operator fun plus(other: Short): Medium = this.plus(other.toMedium())
    operator fun plus(other: Long): Medium = this.plus(other.toMedium())
    operator fun plus(other: Medium): Medium = Medium(this.value.plus(other.value))

    fun toByte(): Byte = value.toByte()
    fun toChar(): Char = value.toChar()
    fun toShort(): Short = value.toShort()
    fun toInt(): Int = value
    fun toLong(): Long = value.toLong()
    fun toFloat(): Float = value.toFloat()
    fun toDouble(): Double = value.toDouble()
    fun toUByte(): UByte = value.toUByte()
    fun toUShort(): UShort = value.toUShort()
    fun toUInt(): UInt = value.toUInt()
    fun toULong(): ULong = value.toULong()
}

fun Byte.toMedium() = Medium(this.toInt())
fun Short.toMedium() = Medium(this.toInt())
fun Long.toMedium() = this.toInt().toMedium()
fun Int.toMedium(): Medium {
    if (this > Medium.MAX_VALUE.value) {
        throw ArithmeticException("too large value")
    } else if (this < Medium.MIN_VALUE.value) {
        throw ArithmeticException("too small value")
    }
    return Medium(this)
}
