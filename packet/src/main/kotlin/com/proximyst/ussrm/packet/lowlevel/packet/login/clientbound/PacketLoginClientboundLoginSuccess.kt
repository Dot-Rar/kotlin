/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.login.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf
import java.util.*

/**
 * This indicates the login, encryption, and compression process is done.
 *
 * Once this has been sent, the new state is [Packet.State.PLAY].
 *
 * The fields are what the server stores on the player and identifies them as.
 */
class PacketLoginClientboundLoginSuccess(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {
    /**
     * The UUID of the player.
     */
    val uuid: UUID by data

    /**
     * The username of the player, as gotten from Mojang.
     */
    val username: String by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        return uuid.toPacket().encode().writeBytes(username.toPacket().encode())
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x02
        override val state = Packet.State.LOGIN
    }
}