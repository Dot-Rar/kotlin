/*
 * Copyright (C) 2018 The USSRM Developers
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf

/**
 * Notifies the client that it should open a window (chest, workbench, furnace, ect)
 * This packet is not used for a client opening their own inventory
 *
 * **For more information, see:**
 * <https://wiki.vg/Inventory>
 */
class PacketPlayClientboundOpenWindow(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The unique ID of the window to be opened
     */
    val windowId: Byte by data

    /**
     * The type of window that should be opened, for example, anvil or furnace
     *
     * **For more information, see:**
     * <https://wiki.vg/Inventory>
     */
    val windowType: String by data

    /**
     * The title of the window
     */
    val title: String by data

    /**
     * The number of slots in the window, excluding the number of slots in the player inventory
     * Always 0 for non-storage windows, such as an anvil or a crafting table
     */
    val slotCount: Byte by data

    /**
     * The numeric ID of the horse entity
     * Only sent if the window is a horse's inventory (windowType = "EntityHorse")
     */
    val entityId: Int? by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        if(windowType.length > 32)
            throw IllegalArgumentException("Window Type cannot be longer than 32 characters")

        val buf = windowId.toPacket().encode(options)
            .writeBytes(windowType.toPacket().encode(options))
            .writeBytes(title.toPacket().encode(options))
            .writeBytes(slotCount.toUByte().toPacket().encode(options))

        entityId?.let { entityId -> buf.writeBytes(entityId.toPacket().encode(options)) }

        return buf
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x14
        override val state = Packet.State.PLAY
    }
}