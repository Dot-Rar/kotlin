/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf


/**
 * The server responds with a list of auto-completions of the last word sent to it.
 * In the case of regular chat, this is a player username.
 * Command names and parameters are also supported.
 * The client sorts these alphabetically before listing them.
 */
class PacketPlayClientboundTabComplete(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * Start of the text to replace
     */
    val start: VarInt by data

    /**
     * Length of the text to replace
     */
    val length: VarInt by data

    /**
     * Number of elements in the {@link #matches matches} array
     */
    val count: VarInt by data

    /**
     * Array of possible completions to be sent to the client
     */
    val matches: MutableList<Match> by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        val buf = start.encode(options)
            .writeBytes(length.encode(options))
            .writeBytes(count.encode(options))

        matches.forEach { (match, hasToolTip, toolTip) ->
            buf.writeBytes(match.toPacket().encode(options))
            buf.writeBytes(hasToolTip.toPacket().encode(options))

            if (hasToolTip)
                buf.writeBytes(toolTip!!.toPacket().encode(options))
        }

        return buf
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x10
        override val state = Packet.State.PLAY
    }

    data class Match(
        /**
         * Possible completion that will be sent to the client.
         * Does not include a '/', even if the match is a command
         */
        val match: String,

        /**
         * True if the completion has a tooltip
         */
        val hasToolTip: Boolean,

        /**
         * Optional chat string (JSON encoded) to be included if hasToolTip is true
         */
        val toolTip: String?
    )
}