/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.data

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.netty.readByteOrNull
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled

class PacketByte(private val byte: Byte) : DataType<PacketByte, Byte> {
    override fun encode(options: Map<String, Any>?): ByteBuf {
        return Unpooled.copiedBuffer(byteArrayOf(byte))
    }

    override fun contained() = byte

    override fun decode(data: ByteBuf) = Companion.decode(data)

    companion object : Decodable<PacketByte> {
        override fun decode(data: ByteBuf): PacketByte? {
            return PacketByte(data.readByteOrNull() ?: return null)
        }
    }
}

fun Byte.toPacket() = PacketByte(this)