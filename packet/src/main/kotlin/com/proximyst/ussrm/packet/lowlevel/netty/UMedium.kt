/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.netty

class UMedium(internal val value: UInt) : Comparable<UMedium> {
    companion object {
        val MIN_VALUE: UMedium = UMedium(0u)
        val MAX_VALUE: UMedium = UMedium(16_777_215u)
        const val SIZE_BYTES: Int = 3
        const val SIZE_BITS: Int = 24
    }

    operator fun compareTo(other: UByte): Int = value.compareTo(other)
    operator fun compareTo(other: UShort): Int = value.compareTo(other)
    operator fun compareTo(other: ULong): Int = value.compareTo(other)
    override operator fun compareTo(other: UMedium): Int = value.compareTo(other.value)

    operator fun plus(other: UByte): UMedium = this.plus(other.toUMedium())
    operator fun plus(other: UShort): UMedium = this.plus(other.toUMedium())
    operator fun plus(other: ULong): UMedium = this.plus(other.toUMedium())
    operator fun plus(other: UMedium): UMedium = UMedium(other.value.plus(value))

    fun toByte(): Byte = value.toByte()
    fun toChar(): Char = toByte().toChar()
    fun toShort(): Short = value.toShort()
    fun toInt(): Int = value.toInt()
    fun toLong(): Long = value.toLong()
    fun toFloat(): Float = toInt().toFloat()
    fun toDouble(): Double = toLong().toDouble()
    fun toUByte(): UByte = value.toUByte()
    fun toUShort(): UShort = value.toUShort()
    fun toUInt(): UInt = value
    fun toULong(): ULong = value.toULong()
}

fun UByte.toUMedium() = UMedium(this.toUInt())
fun UShort.toUMedium() = UMedium(this.toUInt())
fun ULong.toUMedium() = this.toUInt().toUMedium()
fun UInt.toUMedium(): UMedium {
    if (this.toUInt() > UMedium.MAX_VALUE.value) {
        throw ArithmeticException("too large value")
    } else if (this.toUInt() < UMedium.MIN_VALUE.value) {
        throw ArithmeticException("too small value")
    }
    return UMedium(this.toUInt())
}
