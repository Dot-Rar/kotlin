/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.data

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.netty.readUShortOrNull
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled

class PacketUShort(private val short: UShort) : DataType<PacketUShort, UShort>, Decodable<PacketUShort> by Companion {
    override fun encode(options: Map<String, Any>?): ByteBuf {
        return Unpooled.buffer(Short.SIZE_BYTES).writeShort(short.toInt())
    }

    override fun contained() = short

    companion object : Decodable<PacketUShort> {
        override fun decode(data: ByteBuf): PacketUShort? {
            return PacketUShort(data.readUShortOrNull() ?: return null)
        }
    }
}

fun UShort.toPacket() = PacketUShort(this)