/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.status.serverbound

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.PacketLong
import io.netty.buffer.ByteBuf

/**
 * Pings the client with a small payload.
 */
class PacketStatusServerboundPing(
    override val data: MutableMap<String, Any?>
) : Packet, Decodable<PacketStatusServerboundPing> by Companion, PacketData by Companion {
    /**
     * The payload from the client.
     *
     * This may be any value,
     */
    val payload: Long by data

    companion object : PacketData, Decodable<PacketStatusServerboundPing> {
        override val bind = Packet.Bind.SERVER
        override val id = 0x01
        override val state = Packet.State.STATUS

        override fun decode(data: ByteBuf): PacketStatusServerboundPing? {
            val payload = PacketLong.decode(data)?.contained() ?: return null

            return PacketStatusServerboundPing(
                mutableMapOf(
                    "payload" to payload
                )
            )
        }
    }
}