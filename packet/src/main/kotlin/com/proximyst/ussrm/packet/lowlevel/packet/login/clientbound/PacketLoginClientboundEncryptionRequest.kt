/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.login.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import com.proximyst.ussrm.packet.lowlevel.data.toVarInt
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled

/**
 * Requests the client to initiate encryption.
 */
class PacketLoginClientboundEncryptionRequest(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {
    /**
     * The server ID; this can be max 20 characters.
     *
     * After the 1.7 update, this should be empty.
     */
    val serverId: String by data

    /**
     * The public key to use for the connection.
     *
     * It should be a 1024-bit RSA public key in DER format.
     */
    val publicKey: ByteArray by data

    /**
     * The verification token for the client to encrypt and send back together with a shared secret.
     *
     * This is always 4 bytes long for the Notchian server.
     */
    val verifyToken: ByteArray by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        val serverId = this.serverId.take(20).toPacket().encode()

        val buf = Unpooled.buffer(serverId.readableBytes() + publicKey.size + verifyToken.size)
        buf.writeBytes(serverId)
        buf.writeBytes(publicKey.size.toVarInt().encode())
        buf.writeBytes(publicKey)
        buf.writeBytes(verifyToken.size.toVarInt().encode())
        buf.writeBytes(verifyToken)
        return buf
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x01
        override val state = Packet.State.LOGIN
    }
}