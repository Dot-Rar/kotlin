/*
 * Copyright (C) 2018 The USSRM Developers
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf

/**
 * Notifies the client when a window is forcibly closed
 */
class PacketPlayClientboundCloseWindow(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The ID of the window that will be closed
     * If the window is the inventory, it will be 0
     */
    val windowId: Byte by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        return windowId.toUByte().toPacket().encode(options)
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x13
        override val state = Packet.State.PLAY
    }
}