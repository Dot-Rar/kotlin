/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.Position
import io.netty.buffer.ByteBuf

/**
 * Sets the block entity associated with the block at the given location.
 */
class PacketPlayClientboundUpdateBlockEntity(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The location of the block
     */
    val location: Position by data

    /**
     * The type of update to perform. See the link below for the possible values of this field.
     *
     * **For more information, see:**
     * <https://wiki.vg/Protocol#Update_Block_entity>
     */
    val action: Byte by data

    /**
     * NBT tag to apply to the block
     */
    val nbtData: Nothing = TODO("Implement NBT")

    override fun encode(options: Map<String, Any>?): ByteBuf {
        TODO("Implement NBT")

        /*return location.encode(options)
            .writeBytes(action.toPacket().encode(options))
            .writeBytes(nbtData.toPacket().encode(options))*/
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x09
        override val state = Packet.State.PLAY
    }
}