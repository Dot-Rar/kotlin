/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.Position
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf

class PacketPlayClientboundBlockAction(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The location of the block that will perform the action
     */
    val location: Position by data

    /**
     * The ID of the action that will be executed
     *
     * **For more information, see:**
     * <https://wiki.vg/Block_Actions>
     */
    val actionId: UByte by data

    /**
     * Data about the action. For example, it could be a direction
     *
     * **For more information, see:**
     * <https://wiki.vg/Block_Actions>
     */
    val actionParam: UByte by data

    /**
     * The ID for the type of the block. Must match the type of the block at the given location
     */
    val blockType: VarInt by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        return location.encode(options)
            .writeBytes(actionId.toPacket().encode(options))
            .writeBytes(actionParam.toPacket().encode(options))
            .writeBytes(blockType.encode(options))
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x0A
        override val state = Packet.State.PLAY
    }
}