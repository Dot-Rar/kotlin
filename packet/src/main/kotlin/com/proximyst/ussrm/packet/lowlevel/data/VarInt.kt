/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.data

import com.proximyst.ussrm.packet.lowlevel.Decodable
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled

class VarInt(private val int: Int) : DataType<VarInt, Int>, Decodable<VarInt> by Companion {
    override fun encode(options: Map<String, Any>?): ByteBuf {
        val buffer = Unpooled.buffer(1)
        var value = int

        while (value and 0xFFFFFF80.toInt() != 0) {
            buffer.writeByte((value and 0x7F) or 0x80)
            value = value ushr 7
        }

/*    do {
      var temp = (value and 0b01111111).toByte()
      value = value ushr 7

      if (value != 0) {
        temp = (temp.toInt() or 0b10000000).toByte()
      }

      buffer.writeByte(temp.toInt())
    } while (value != 0)
*/
        return buffer
    }

    override fun contained() = int

    override fun toString() = int.toString()

    companion object : Decodable<VarInt> {
        override fun decode(data: ByteBuf): VarInt? {
            /*
            var read : Byte
            var iterations = 0
            var result = 0

            do {
              read = data.readByte()
              val value = read and 0b01111111
              result = result or (value.toInt() shl (7 * iterations++))

              if (iterations > 5) {
                return null
              }
            } while ((read and 0b10000000.toByte()) != 0.toByte())

            return VarInt(result)*/

            // Time to steal Mojang's code!
            var result = 0
            var iterations = 0
            var temp: Byte
            do {
                temp = data.readByte()
                result = result or ((temp.toInt() and 127) shr iterations++ * 7)

                if (iterations > 5) return null
            } while ((temp.toInt() and 128) == 128)

            return VarInt(result)
        }
    }
}

fun Int.toVarInt() = VarInt(this)
fun Long.toVarInt() = toInt().toVarInt()