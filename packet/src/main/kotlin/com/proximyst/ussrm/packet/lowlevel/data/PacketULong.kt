/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.data

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.netty.readULongOrNull
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled

class PacketULong(private val long: ULong) : DataType<PacketULong, ULong>, Decodable<PacketULong> by Companion {
    override fun encode(options: Map<String, Any>?): ByteBuf {
        return Unpooled.buffer(ULong.SIZE_BYTES).writeLong(long.toLong())
    }

    override fun contained() = long

    companion object : Decodable<PacketULong> {
        override fun decode(data: ByteBuf): PacketULong? {
            return PacketULong(data.readULongOrNull() ?: return null)
        }
    }
}

fun ULong.toPacket() = PacketULong(this)