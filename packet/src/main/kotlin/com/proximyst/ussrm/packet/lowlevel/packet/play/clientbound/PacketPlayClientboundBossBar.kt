/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import com.proximyst.ussrm.packet.lowlevel.data.play.bossbar.*
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf
import java.util.*

class PacketPlayClientboundBossBar(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The UUID of the boss bar
     */
    val uuid: UUID by data

    /**
     * ID of the boss bar action
     */
    val actionId: VarInt by data

    /**
     * Build action wrapper
     */
    val action: BossBarAction =
        when (actionId.contained()) {
            0 -> object : AddAction {
                override val title: String by data
                override val health: Float by data

                private val _colorId: VarInt by data
                private val _divisionId: VarInt by data
                private val _flags: Byte by data

                override val color = Colors.values().first { it.id == _colorId.contained() }
                override val division = Division.values().first { it.id == _divisionId.contained() }
                override val flags = _flags.toUByte()
            }

            1 -> object : RemoveAction {}

            2 -> object : UpdateHealthAction {
                override val health: Float by data
            }

            3 -> object : UpdateTitleAction {
                override val title: String by data
            }

            4 -> object : UpdateStyleAction {
                private val _colorId: VarInt by data
                private val _divisionId: VarInt by data

                override val color = Colors.values().first { it.id == _colorId.contained() }
                override val division = Division.values().first { it.id == _divisionId.contained() }
            }

            5 -> object : UpdateFlagsAction {
                private val _flags: Byte by data

                override val flags = _flags.toUByte()
            }

            else -> throw IllegalArgumentException("Invalid action ID")
        }

    override fun encode(options: Map<String, Any>?): ByteBuf {
        val buf = uuid.toPacket().encode(options)
            .writeBytes(actionId.encode(options))

        when(action) {
            is AddAction -> {
                buf.writeBytes(action.title.toPacket().encode(options))
                    .writeBytes(action.health.toPacket().encode(options))
                    .writeBytes(action.color.id.toPacket().encode(options))
                    .writeBytes(action.division.id.toPacket().encode(options))
                    .writeBytes(action.flags.toPacket().encode(options))
            }

            is UpdateHealthAction -> {
                buf.writeBytes(action.health.toPacket().encode(options))
            }

            is UpdateTitleAction -> {
                buf.writeBytes(action.title.toPacket().encode(options))
            }

            is UpdateStyleAction -> {
                buf.writeBytes(action.color.id.toPacket().encode(options))
                    .writeBytes(action.division.id.toPacket().encode(options))
            }

            is UpdateFlagsAction -> {
                buf.writeBytes(action.flags.toPacket().encode(options))
            }
        }

        return buf
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x0C
        override val state = Packet.State.PLAY
    }
}