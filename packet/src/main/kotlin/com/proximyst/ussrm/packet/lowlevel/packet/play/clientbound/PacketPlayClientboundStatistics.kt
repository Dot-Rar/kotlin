/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import io.netty.buffer.ByteBuf

class PacketPlayClientboundStatistics(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The amount of elements in the {@link #statistic statistics} array
     */
    val count: VarInt by data

    /**
     * Array of statistics to be updated.
     * First element is the ID of the caregory (e.g. mined, crafted, used)
     * Second element is the ID of the statistic (e.g. leave_game, play_one_minute)
     *
     * **For more information, see:**
     * <https://wiki.vg/Protocol#Statistics>
     */
    val statistic: MutableSet<Statistic> by data

    /**
     * The new value of the statistic
     */
    val value: VarInt by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        val byteBuf = count.encode(options)
        statistic.forEach { (categoryId, statisticId) ->
            byteBuf.writeBytes(categoryId.encode(options))
            byteBuf.writeBytes(statisticId.encode(options))
        }
        byteBuf.writeBytes(value.encode(options))

        return byteBuf
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x07
        override val state = Packet.State.PLAY
    }

    data class Statistic(val categoryId: VarInt, val statisticId: VarInt)
}
