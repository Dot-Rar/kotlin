/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.data

import com.proximyst.ussrm.packet.lowlevel.Decodable
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import kotlin.experimental.and
import kotlin.experimental.or

class VarLong(private val long: Long) : DataType<VarLong, Long>, Decodable<VarLong> by Companion {
    override fun encode(options: Map<String, Any>?): ByteBuf {
        val buffer = Unpooled.buffer(1)
        var original = long

        do {
            var temp = (original and 0b01111111).toByte()
            original = original ushr 7

            if (original != 0.toLong()) {
                temp = temp or 0b10000000.toByte()
            }

            buffer.writeByte(temp.toInt())
        } while (original != 0.toLong())

        return buffer
    }

    override fun contained() = long

    companion object : Decodable<VarLong> {
        override fun decode(data: ByteBuf): VarLong? {
            var read: Byte
            var iterations = 0
            var result: Long = 0

            do {
                read = data.readByte()
                val value = read and 0b01111111
                result = result or (value.toLong() shl (7 * iterations++))

                if (iterations > 10) {
                    return null
                }
            } while ((read and 0b10000000.toByte()) != 0.toByte())

            return VarLong(result)
        }
    }
}

fun Int.toVarLong() = this.toLong().toVarLong()
fun Long.toVarLong() = VarLong(this)