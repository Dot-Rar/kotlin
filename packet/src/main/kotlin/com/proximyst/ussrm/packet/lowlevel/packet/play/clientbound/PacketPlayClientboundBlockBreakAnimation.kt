/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.Position
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf

/**
 * Notifies the client on what stage of the block break animation a block is currently on
 */
class PacketPlayClientboundBlockBreakAnimation(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The numeric ID of the entity breaking the block
     */
    val entityId: VarInt by data

    /**
     * The location of the block
     */
    val location: Position by data

    /**
     * The current stage of the block break animation, between 0 and 9.
     * If the value specified is out of the range 0-9 (inclusive), then the block will be removed
     */
    val destroyStage: Byte by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        return entityId.encode(options)
            .writeBytes(location.encode(options))
            .writeBytes(destroyStage.toPacket().encode(options))
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x08
        override val state = Packet.State.PLAY
    }
}