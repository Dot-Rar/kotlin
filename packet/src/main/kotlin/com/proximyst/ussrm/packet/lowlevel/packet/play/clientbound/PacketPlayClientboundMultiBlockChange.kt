/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf

/**
 * Fired whenever 2 or more blocks are changed within the same chunk on the same tick.
 */
class PacketPlayClientboundMultiBlockChange(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * X coordinate of the chunk
     */
    val chunkX: Int by data

    /**
     * Z coordinate of the chunk
     */
    val chunkZ: Int by data

    /**
     * Number of elements in the record array
     */
    val recordCount: VarInt by data

    /**
     * Block change records to will be sent to the client
     */
    val records: MutableList<Record> by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        val buf = chunkX.toPacket().encode(options)
            .writeBytes(chunkZ.toPacket().encode(options))
            .writeBytes(recordCount.encode(options))

        records.forEach { (horizontalPosition, y, blockId) ->
            buf.writeBytes(horizontalPosition.toPacket().encode(options))
            buf.writeBytes(y.toPacket().encode(options))
            buf.writeBytes(blockId.encode(options))
        }

        return buf
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x0F
        override val state = Packet.State.PLAY
    }

    data class Record(val horizontalPosition: UByte, val y: UByte, val blockId: VarInt)
}