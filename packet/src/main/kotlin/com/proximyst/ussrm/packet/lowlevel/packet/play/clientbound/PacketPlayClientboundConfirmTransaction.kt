/*
 * Copyright (C) 2018 The USSRM Developers
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf

/**
 * Notifies the client of whether their action was accepted
 * If the action was denied, the client must send a serverbound confirm transaction packet
 */
class PacketPlayClientboundConfirmTransaction(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The ID of the window that the action occurred in
     */
    val windowId: Byte by data

    /**
     * The unique ID of the action.
     * Each window has a separate counter, starting from 0
     */
    val actionNumber: Short by data

    /**
     * Whether the action was accepted
     */
    val accepted: Boolean by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        return windowId.toPacket().encode(options)
            .writeBytes(actionNumber.toPacket().encode(options))
            .writeBytes(accepted.toPacket().encode(options))
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x12
        override val state = Packet.State.PLAY
    }
}