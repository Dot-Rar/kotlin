/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel

import io.netty.buffer.ByteBuf

/**
 * [Encodable] represents a type which can be encoded into a [ByteBuf].
 */
interface Encodable {
    /**
     * Encodes the type into a [ByteBuf] with the [options] applied.
     *
     * @param options
     *    The options to follow during encoding.
     *
     * @return
     *    The encoded [ByteBuf] of the type.
     */
    fun encode(options: Map<String, Any>? = null): ByteBuf
}