/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.netty

import io.netty.buffer.ByteBuf

fun <T> ByteBuf.readSafely(size: Int, default: T? = null, block: ByteBuf.() -> T?) =
    if (isReadable(size))
        block()
    else
        default

fun ByteBuf.readByteOrNull() =
    readSafely(Byte.SIZE_BYTES) {
        readByte()
    }

fun ByteBuf.readUByteOrNull() =
    readSafely(UByte.SIZE_BYTES) {
        readUByte()
    }

fun ByteBuf.readShortOrNull() =
    readSafely(Short.SIZE_BYTES) {
        readShort()
    }

fun ByteBuf.readUShortOrNull() =
    readSafely(UShort.SIZE_BYTES) {
        readUShort()
    }

fun ByteBuf.readShortLEOrNull() =
    readSafely(Short.SIZE_BYTES) {
        readShortLE()
    }

fun ByteBuf.readUShortLEOrNull() =
    readSafely(UShort.SIZE_BYTES) {
        readUShortLE()
    }

fun ByteBuf.readMediumKtOrNull() =
    readSafely(Medium.SIZE_BYTES) {
        readMediumKt()
    }

fun ByteBuf.readUMediumKtOrNull() =
    readSafely(UMedium.SIZE_BYTES) {
        readUMediumKt()
    }

fun ByteBuf.readMediumKtLEOrNull() =
    readSafely(Medium.SIZE_BYTES) {
        readMediumLEKt()
    }

fun ByteBuf.readUMediumKtLEOrNull() =
    readSafely(UMedium.SIZE_BYTES) {
        readUMediumLEKt()
    }

fun ByteBuf.readIntOrNull() =
    readSafely(Int.SIZE_BYTES) {
        readInt()
    }

fun ByteBuf.readUIntOrNull() =
    readSafely(UInt.SIZE_BYTES) {
        readUInt()
    }

fun ByteBuf.readIntLEOrNull() =
    readSafely(Int.SIZE_BYTES) {
        readIntLE()
    }

fun ByteBuf.readUIntLEOrNull() =
    readSafely(UInt.SIZE_BYTES) {
        readUIntLE()
    }

fun ByteBuf.readLongOrNull() =
    readSafely(Long.SIZE_BYTES) {
        readLong()
    }

fun ByteBuf.readULongOrNull() =
    readSafely(ULong.SIZE_BYTES) {
        readULong()
    }

fun ByteBuf.readLongLEOrNull() =
    readSafely(Long.SIZE_BYTES) {
        readLongLE()
    }

fun ByteBuf.readULongLEOrNull() =
    readSafely(ULong.SIZE_BYTES) {
        readULongLE()
    }

fun ByteBuf.readFloatOrNull() =
    readSafely(4) {
        readFloat()
    }

fun ByteBuf.readFloatLEOrNull() =
    readSafely(4) {
        readFloatLE()
    }

fun ByteBuf.readDoubleOrNull() =
    readSafely(8) {
        readDouble()
    }

fun ByteBuf.readDoubleLEOrNull() =
    readSafely(8) {
        readDoubleLE()
    }

fun ByteBuf.readUByte(): UByte =
    readUnsignedByte().toUByte()

fun ByteBuf.readUShort(): UShort =
    readUnsignedShort().toUShort()

fun ByteBuf.readUShortLE(): UShort =
    readUnsignedShortLE().toUShort()

fun ByteBuf.readMediumKt(): Medium =
    readMedium().toMedium()

fun ByteBuf.readMediumLEKt(): Medium =
    readMediumLE().toMedium()

fun ByteBuf.readUMediumKt(): UMedium =
    readUnsignedMedium().toUInt().toUMedium()

fun ByteBuf.readUMediumLEKt(): UMedium =
    readUnsignedMediumLE().toUInt().toUMedium()

fun ByteBuf.readUInt(): UInt =
    readUnsignedInt().toUInt()

fun ByteBuf.readUIntLE(): UInt =
    readUnsignedIntLE().toUInt()

fun ByteBuf.readULong(): ULong =
    readLong().toULong()

fun ByteBuf.readULongLE(): ULong =
    readLongLE().toULong()
