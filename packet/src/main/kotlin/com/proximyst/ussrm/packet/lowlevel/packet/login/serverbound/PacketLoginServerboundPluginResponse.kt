/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.login.serverbound

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.PacketBoolean
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import io.netty.buffer.ByteBuf

/**
 * Always received after a plugin request. This contains the data from the request.
 *
 * The Notchian client will always send an empty response.
 */
class PacketLoginServerboundPluginResponse(
    override val data: MutableMap<String, Any?>
) : Packet, Decodable<PacketLoginServerboundPluginResponse> by Companion, PacketData by Companion {
    /**
     * The message ID as sent in the request.
     *
     * An unknown ID is an illegal packet.
     */
    val messageId: Int by data

    /**
     * Whether the client understood the request. This dictates [payload]'s null state.
     *
     * The Notchian client always sends false.
     */
    val successful: Boolean by data

    /**
     * The payload sent with the response. This is only an array if [successful] is true.
     */
    val payload: ByteArray? by data

    companion object : PacketData, Decodable<PacketLoginServerboundPluginResponse> {
        override val bind = Packet.Bind.SERVER
        override val id = 0x02
        override val state = Packet.State.LOGIN

        override fun decode(data: ByteBuf): PacketLoginServerboundPluginResponse? {
            val messageId = VarInt.decode(data)?.contained() ?: return null
            val successful = PacketBoolean.decode(data)?.contained() ?: return null
            var payload: ByteArray? = null
            if (successful) {
                payload = data.readBytes(data.readableBytes()).array()
            }

            return PacketLoginServerboundPluginResponse(
                mutableMapOf(
                    "messageId" to messageId,
                    "successful" to successful,
                    "payload" to payload
                )
            )
        }
    }
}