/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel

/**
 * A packet which contains data about itself and its properties.
 */
interface Packet : PacketData {
    /**
     * The end of the line which is bound to receive a packet.
     */
    enum class Bind {
        /**
         * The server is the receiver of the packet.
         */
        SERVER,

        /**
         * The client is the receiver of the packet.
         */
        CLIENT,
    }

    /**
     * The state of which the packet is set to.
     */
    enum class State {
        HANDSHAKE,
        LOGIN,
        STATUS,
        PLAY,
    }

    /**
     * All the data in this packet.
     */
    val data: MutableMap<String, Any?>
}