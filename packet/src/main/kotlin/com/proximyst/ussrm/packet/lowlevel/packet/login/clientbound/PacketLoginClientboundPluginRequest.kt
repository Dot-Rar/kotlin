/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.login.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.Identifier
import com.proximyst.ussrm.packet.lowlevel.data.toPacket
import io.netty.buffer.ByteBuf

/**
 * Used to implement custom handshakes with responses.
 *
 * The Notchian client will never understand the payload and always
 * send back an empty response packet.
 */
class PacketLoginClientboundPluginRequest(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {
    /**
     * The message ID for this specific request.
     *
     * This should be randomly generated and unique to the connection.
     */
    val messageId: Int by data

    /**
     * The name of the plugin channel.
     */
    val channel: Identifier by data

    /**
     * The payload to send to the player. This can be empty (see empty [byteArrayOf]).
     */
    val payload: ByteArray by data

    override fun encode(options: Map<String, Any>?): ByteBuf {
        return messageId.toPacket()
            .encode()
            .writeBytes(channel.encode())
            .writeBytes(payload) // the length is inferred from the packet length
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x04
        override val state = Packet.State.LOGIN
    }
}