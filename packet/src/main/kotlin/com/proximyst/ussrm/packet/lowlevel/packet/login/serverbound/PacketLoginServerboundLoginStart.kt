/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.login.serverbound

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.PacketString
import io.netty.buffer.ByteBuf

/**
 * Starts the login process for the client.
 *
 * This gets sent by the client right after the handshake packet.
 */
class PacketLoginServerboundLoginStart(
    override val data: MutableMap<String, Any?>
) : Packet, Decodable<PacketLoginServerboundLoginStart> by Companion, PacketData by Companion {
    /**
     * The username of the player trying to log in.
     *
     * It must be maximum 16 characters.
     */
    val username: String by data

    companion object : PacketData, Decodable<PacketLoginServerboundLoginStart> {
        override val bind = Packet.Bind.SERVER
        override val id = 0x00
        override val state = Packet.State.LOGIN

        override fun decode(data: ByteBuf): PacketLoginServerboundLoginStart? {
            val username = PacketString.decode(data)?.contained() ?: return null
            if (username.length > 16)
                return null

            return PacketLoginServerboundLoginStart(
                mutableMapOf(
                    "username" to username
                )
            )
        }
    }
}