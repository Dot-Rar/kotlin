/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.login.serverbound

import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import io.netty.buffer.ByteBuf

/**
 * Sent when encryption has been established, and contains the shared secret key to use
 * to encrypt everything from this point on.
 *
 * Everything in this packet is encrypted with the server's public key.
 */
class PacketLoginServerboundEncryptionResponse(
    override val data: MutableMap<String, Any?>
) : Packet, Decodable<PacketLoginServerboundEncryptionResponse> by Companion, PacketData by Companion {
    /**
     * The shared symmetric secret key to use for encrypting all packets from here
     * on out which are for this player.
     */
    val sharedSecret: ByteArray by data

    /**
     * The verification token sent in the encryption request.
     * The decrypted token (using the server's private key) must be the same as in the request.
     */
    val verifyToken: ByteArray by data

    companion object : PacketData, Decodable<PacketLoginServerboundEncryptionResponse> {
        override val bind = Packet.Bind.SERVER
        override val id = 0x01
        override val state = Packet.State.LOGIN

        override fun decode(data: ByteBuf): PacketLoginServerboundEncryptionResponse? {
            val sharedSecretLength = VarInt.decode(data)?.contained() ?: return null
            if (data.readableBytes() < sharedSecretLength) return null
            val sharedSecret = data.readBytes(sharedSecretLength)

            val verifyTokenLength = VarInt.decode(data)?.contained() ?: return null
            if (data.readableBytes() < verifyTokenLength) return null
            val verifyToken = data.readBytes(verifyTokenLength)

            return PacketLoginServerboundEncryptionResponse(
                mutableMapOf(
                    "sharedSecret" to sharedSecret,
                    "verifyToken" to verifyToken
                )
            )
        }
    }
}