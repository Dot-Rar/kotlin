/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.packet.play.clientbound

import com.proximyst.ussrm.packet.lowlevel.Encodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.PacketAngle
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import io.netty.buffer.ByteBuf
import java.util.*

/**
 * Notifies the client that a mob entity has spawned
 */
class PacketPlayClientboundSpawnMob(
    override val data: MutableMap<String, Any?>
) : Packet, Encodable, PacketData by Companion {

    /**
     * The numeric ID of the experience orb
     */
    val entityId: VarInt by data

    /**
     * The UUID of the entity
     */
    val uuid: UUID by data

    /**
     * The type of mob
     *
     * For more info: 
     * <https://wiki.vg/Entity_metadata#Mobs
     */
    val type: VarInt by data

    /**
     * The X coordinate of the entity
     */
    val x: Double by data

    /**
     * The Y coordinate of the entity
     */
    val y: Double by data

    /**
     * The Z coordinate of the entity
     */
    val z: Double by data

    /**
     * The angle of the yaw of the entity
     */
    val yaw: PacketAngle by data


    /**
     * The angle of the pitch of the entity
     */
    val pitch: PacketAngle by data

    /**
     * The angle of the pitch of the entity's head
     */
    val headPitch: PacketAngle by data

    /**
     * The entity's velocity on the X axis
     */
    val velocityX: Short by data

    /**
     * The entity's velocity on the Y axis
     */
    val velocityY: Short by data

    /**
     * The entity's velocity on the Z axis
     */
    val velocityZ: Short by data

    val metadata: Nothing = TODO("Entity metadata enum has not been implemented")

    override fun encode(options: Map<String, Any>?): ByteBuf {
        //TODO: Write metadata
        TODO("Entity metadata enum has not been implemented")

        /*return entityId.encode(options)
            .writeBytes(uuid.toPacket().encode(options))
            .writeBytes(type.encode(options))
            .writeBytes(x.toPacket().encode(options))
            .writeBytes(y.toPacket().encode(options))
            .writeBytes(z.toPacket().encode(options))
            .writeBytes(yaw.encode(options))
            .writeBytes(pitch.encode(options))
            .writeBytes(headPitch.encode(options))
            .writeBytes(velocityX.toPacket().encode(options))
            .writeBytes(velocityY.toPacket().encode(options))
            .writeBytes(velocityZ.toPacket().encode(options))*/
    }

    companion object : PacketData {
        override val bind = Packet.Bind.CLIENT
        override val id = 0x03
        override val state = Packet.State.PLAY
    }
}
