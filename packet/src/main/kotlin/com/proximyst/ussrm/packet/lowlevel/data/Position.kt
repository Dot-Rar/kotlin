/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel.data

import com.proximyst.ussrm.packet.lowlevel.Decodable
import io.netty.buffer.ByteBuf

typealias CoordTriple = Triple<Int, Short, Int>

class Position(val coordinates: CoordTriple) : DataType<Position, CoordTriple>, Decodable<Position> by Companion {
    constructor(x: Number, y: Number, z: Number) : this(Triple(x.toInt(), y.toShort(), z.toInt()))

    override fun encode(options: Map<String, Any>?): ByteBuf {
        val (x, y, z) = coordinates

        if (x < -33554432 || x > 33554431)
            throw IllegalStateException("invalid X coordinate")
        else if (y < -2048 || y > 2047)
            throw IllegalStateException("invalid Y coordinate")
        else if (z < -33554432 || z > 33554431)
            throw IllegalStateException("invalid Z coordinate")

        return PacketULong(

            ((x and 0x3FFFFFF).toULong() shl 38)

                    or ((y.toInt() and 0xFFF).toULong() shl 26)

                    or (z.toULong() and 0x3FFFFFF.toULong())

        ).encode()
    }

    override fun contained() = coordinates

    companion object : Decodable<Position> {
        override fun decode(data: ByteBuf): Position? {
            val long = PacketULong.decode(data)?.contained() ?: return null
            var x = (long shr 38).toInt()
            var y = ((long shr 26) and 0xFFF.toULong()).toShort()
            var z = ((long shl 36) shr 38).toInt()

            if (x >= 33_554_432) // 2 ** 25
                x -= 67_108_864 // 2 ** 26
            if (y >= 2048) // 2 ** 11
                y = (y - 4096).toShort() // 2 ** 12 - kotlin bug doesn't allow `y -= 4096.toShort()`
            if (z >= 33_554_432) // 2 ** 25
                z -= 67_108_864 // 2 ** 26

            return Position(x, y, z)
        }
    }
}