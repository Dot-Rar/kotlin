/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.packet.lowlevel

import io.netty.buffer.ByteBuf

/**
 * [Decodable] represents something which can be decoded into [T].
 */
interface Decodable<T> {
    /**
     * Tries to decode the [T] out of the [ByteBuf], but returns null on failure.
     *
     * @param data
     *    The [ByteBuf] of the data.
     *
     * @return
     *    The decoded [T] or null if an error occurred.
     */
    fun decode(data: ByteBuf): T?
}