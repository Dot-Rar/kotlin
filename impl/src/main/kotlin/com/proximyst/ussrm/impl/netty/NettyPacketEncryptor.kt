/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.impl.netty

import com.proximyst.ussrm.api.network.PlayerConnection
import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToByteEncoder

/**
 * Encrypts all data given to it using [connection]'s settings.
 */
class NettyPacketEncryptor(
    /**
     * The [PlayerConnection] to encrypt for.
     */
    val connection: PlayerConnection
) : MessageToByteEncoder<ByteBuf>() {
    override fun encode(ctx: ChannelHandlerContext?, msg: ByteBuf, out: ByteBuf) {
        if (!connection.encryptionSettings.enabled) {
            out.writeBytes(msg)
            return
        }

        val array = ByteArray(msg.readableBytes())
        msg.readBytes(array)
        out.writeBytes(connection.encryptionSettings.encryptionCipher?.doFinal(array) ?: return)
    }
}
