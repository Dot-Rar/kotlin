/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.impl.netty

import com.proximyst.ussrm.api.network.PlayerConnection
import com.proximyst.ussrm.impl.SovietServer
import com.proximyst.ussrm.packet.lowlevel.Decodable
import com.proximyst.ussrm.packet.lowlevel.Packet
import com.proximyst.ussrm.packet.lowlevel.PacketData
import com.proximyst.ussrm.packet.lowlevel.data.VarInt
import com.proximyst.ussrm.packet.lowlevel.packet.handshake.serverbound.PacketHandshakeServerboundHandshake
import com.proximyst.ussrm.packet.lowlevel.packet.login.serverbound.PacketLoginServerboundEncryptionResponse
import com.proximyst.ussrm.packet.lowlevel.packet.login.serverbound.PacketLoginServerboundLoginStart
import com.proximyst.ussrm.packet.lowlevel.packet.login.serverbound.PacketLoginServerboundPluginResponse
import com.proximyst.ussrm.packet.lowlevel.packet.status.serverbound.PacketStatusServerboundPing
import com.proximyst.ussrm.packet.lowlevel.packet.status.serverbound.PacketStatusServerboundRequest
import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.ByteToMessageDecoder

/**
 * Decodes all packets given to it to proper packets and then passes them to [SovietServer.lowLevelPacketManager].
 */
class NettyPacketDecoder(
    /**
     * The [PlayerConnection] this decoder handles for.
     */
    val connection: PlayerConnection
) : ByteToMessageDecoder() {
    override fun decode(ctx: ChannelHandlerContext, buf: ByteBuf, out: MutableList<Any>) {
        if (buf.readableBytes() == 0) return

        val packetId = VarInt.decode(buf) ?: return
        val decoder = packetDecoders[connection.state to packetId.contained()] ?: return
        val decoded = decoder.decode(buf) ?: return
        if (buf.readableBytes() > 0) return
        out.add(decoded)
        SovietServer.lowLevelPacketManager.handle(decoded, ctx, connection)
    }

    companion object {
        val packetDecoders = mutableMapOf<Pair<Packet.State, Int>, Decodable<out Packet>>()

        init {
            // HANDSHAKE: SERVERBOUND
            decoder(PacketHandshakeServerboundHandshake)

            // LOGIN: SERVERBOUND
            decoder(PacketLoginServerboundLoginStart)
            decoder(PacketLoginServerboundEncryptionResponse)
            decoder(PacketLoginServerboundPluginResponse)

            // PLAY: SERVERBOUND

            // STATUS: SERVERBOUND
            decoder(PacketStatusServerboundRequest)
            decoder(PacketStatusServerboundPing)
        }

        private inline fun <reified A, reified B> decoder(companion: B)
                where A : Packet,
                      B : Decodable<A>,
                      B : PacketData {
            packetDecoders[companion.state to companion.id] = companion
        }
    }
}