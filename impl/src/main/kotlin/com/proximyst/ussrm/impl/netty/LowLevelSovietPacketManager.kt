/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.impl.netty

import com.proximyst.ussrm.api.Server
import com.proximyst.ussrm.api.network.LowLevelIncomingPacketHandler
import com.proximyst.ussrm.api.network.LowLevelPacketManager
import com.proximyst.ussrm.api.network.PlayerConnection
import com.proximyst.ussrm.impl.SovietServer
import com.proximyst.ussrm.packet.lowlevel.Packet
import io.netty.channel.ChannelHandlerContext

/**
 * Handles packet handlers and the packets themselves.
 *
 * One should get the instance of this through [Server] or [SovietServer].
 */
class LowLevelSovietPacketManager : LowLevelPacketManager {
    /**
     * All the packet handlers and their respective packets.
     */
    private val handlers = mutableMapOf<Class<out Packet>, MutableSet<LowLevelIncomingPacketHandler<Packet>>>()

    /**
     * Listens to the packet [T] using [handler].
     *
     * @param T
     *      The type of the packet to listen to.
     *
     * @param type
     *      The [Class] instance of [T].
     *
     * @param handler
     *      The [LowLevelIncomingPacketHandler] which takes [T].
     *
     * @see handlers
     */
    override fun <T : Packet> listen(type: Class<T>, handler: LowLevelIncomingPacketHandler<out T>) {
        handlers[type] = (handlers[type] ?: mutableSetOf()).apply {
            @Suppress("UNCHECKED_CAST") // Must always be of type Packet anyway; this can't ever fail w/o a modified JVM
            add(handler as LowLevelIncomingPacketHandler<Packet>)
        }
    }

    /**
     * Handles the [packet] of type [T] using the given [channelContext] and [connection].
     *
     * @param T
     *      The type of the packet to handle.
     *
     * @param packet
     *      The packet to handle using all handlers under [handlers] registered to it.
     *
     * @param channelContext
     *      The context to give to all handlers.
     *
     * @param connection
     *      The player's connection to give to all handlers.
     */
    override fun <T : Packet> handle(packet: T, channelContext: ChannelHandlerContext, connection: PlayerConnection) {
        handlers[packet.javaClass]?.forEach {
            it.handle(packet, channelContext, connection)
        }
    }
}