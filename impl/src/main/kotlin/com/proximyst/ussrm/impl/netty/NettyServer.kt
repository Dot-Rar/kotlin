/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.impl.netty

import com.proximyst.ussrm.impl.SovietServer
import com.proximyst.ussrm.impl.listen.packet.HandshakeListen
import com.proximyst.ussrm.packet.lowlevel.packet.handshake.serverbound.PacketHandshakeServerboundHandshake
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel

/**
 * A server handling Netty's different components and the packet listeners.
 */
object NettyServer {
    /**
     * The nio group for workers for the server. These handle packets.
     */
    val workerGroup = NioEventLoopGroup()

    /**
     * The nio group which owns the worker groups.
     */
    val bossGroup = NioEventLoopGroup()

    /**
     * The netty server itself.
     *
     * This should rarely be touched.
     */
    val bootstrap = ServerBootstrap()

    /**
     * Starts the netty server which binds and listens for connections.
     */
    fun start() {
        SovietServer.lowLevelPacketManager.listen(PacketHandshakeServerboundHandshake::class.java, HandshakeListen)

        try {
            bootstrap
                .group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel::class.java)
                .childHandler(NettyChannelInitializer)
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true)

            bootstrap.bind(25565).sync().channel().closeFuture().sync()
        } finally {
            bossGroup.shutdownGracefully()
            workerGroup.shutdownGracefully()
        }
    }
}