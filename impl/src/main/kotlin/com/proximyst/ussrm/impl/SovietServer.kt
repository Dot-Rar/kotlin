/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.impl

import ch.qos.logback.classic.LoggerContext
import com.proximyst.ussrm.api.Server
import com.proximyst.ussrm.api.log.logger
import com.proximyst.ussrm.impl.netty.LowLevelSovietPacketManager
import com.proximyst.ussrm.impl.netty.NettyServer
import org.slf4j.LoggerFactory

/**
 * The server of USSRM, which implements [Server] such that it can be used for [Server.ServerInstanceHolder.server],
 * and for seamless use through [Server.Companion].
 */
object SovietServer : Server {
    override val protocolVersion = 401
    override val protocolName = "1.13.1"
    override val lowLevelPacketManager = LowLevelSovietPacketManager()

    /**
     * Starts the server itself and starts Netty alongside it.
     */
    fun start() {
        Thread {
            NettyServer.start()
        }.apply {
            name = "Netty"
            isDaemon = false
            start()
        }

        Runtime.getRuntime().addShutdownHook(Thread {
            this@SovietServer.logger.info("Shutdown signal has been received.")
            stop()
        })
    }

    /**
     * Stops the server and all of its services.
     *
     * This should only be called by [Runtime] upon shutdown.
     */
    private fun stop() {
        logger.info("Shutdown method has been called; shutting down!")

        (LoggerFactory.getILoggerFactory() as LoggerContext).stop()
    }
}