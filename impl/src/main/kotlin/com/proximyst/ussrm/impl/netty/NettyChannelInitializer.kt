/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.impl.netty

import com.proximyst.ussrm.api.network.PlayerConnection
import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.timeout.ReadTimeoutHandler

/**
 * Initialises the pipeline of the [SocketChannel] it is provided with all data handlers.
 */
object NettyChannelInitializer : ChannelInitializer<SocketChannel>() {
    override fun initChannel(ch: SocketChannel) {
        val connection = PlayerConnection(ch, ch.remoteAddress())

        ch.pipeline().also {
            // CLIENT SIDE //

            // Time out after 30 seconds
            it.addLast(ReadTimeoutHandler(30))

            // Handle legacy pings
            it.addLast("legacy_ping", LegacyPingHandler())

            // Splits the packets
            it.addLast("splitter", NettyPacketSplitter())

            // Decode all packets incoming:
            it.addLast("decoder", NettyPacketDecoder(connection))


            // SERVER SIDE //

            // Encode packets given to the connection:
            it.addLast("encoder", NettyPacketEncoder())

            // Compresses the packets given to the connection:
            it.addLast("compressor", NettyPacketCompressor(connection))

            // Make sure the length of the packet given to the connection is marked:
            it.addLast("length_marker", NettyPacketLengthMarker())

            // Encrypt the entire packet given to the connection:
            it.addLast("encryptor", NettyPacketEncryptor(connection))
        }
    }
}