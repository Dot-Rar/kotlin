/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.impl.listen.packet

import com.proximyst.ussrm.api.network.LowLevelIncomingPacketHandler
import com.proximyst.ussrm.api.network.PlayerConnection
import com.proximyst.ussrm.packet.lowlevel.packet.handshake.serverbound.PacketHandshakeServerboundHandshake
import io.netty.channel.ChannelHandlerContext

/**
 * Listens to [PacketHandshakeServerboundHandshake] and sets the correct data in the [PlayerConnection].
 */
object HandshakeListen : LowLevelIncomingPacketHandler<PacketHandshakeServerboundHandshake> {
    override fun handle(
        packet: PacketHandshakeServerboundHandshake,
        channelContext: ChannelHandlerContext,
        connection: PlayerConnection
    ) {
        connection.state = packet.nextState
        connection.protocolVersion = packet.protocolVersion
        connection.connectingHost = packet.serverAddress
        connection.connectingPort = packet.serverPort
    }
}