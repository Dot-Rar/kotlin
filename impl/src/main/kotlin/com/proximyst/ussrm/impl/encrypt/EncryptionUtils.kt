/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.proximyst.ussrm.impl.encrypt

import io.netty.util.internal.StringUtil
import java.security.*
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * Generator for a keypair using RSA-1024, for use in encryption.
 */
private val generator = KeyPairGenerator.getInstance("RSA").apply { initialize(1024) }

/**
 * A secure random meant for anything which requires it.
 */
internal val secureRandom = SecureRandom()

/**
 * The keypair used for the server.
 */
internal val keyPair = generator.genKeyPair()

/**
 * The private key used on the server to initiate encryption.
 */
internal val privateKey: PrivateKey
    get() = keyPair.private

/**
 * The public key used on the server to initiate encryption.
 */
internal val publicKey: PublicKey
    get() = keyPair.public

/**
 * The PEM formatted version of this public key.
 */
val PublicKey.asPem: String
    get() = buildString {
        append("-----BEGIN PUBLIC KEY-----")
        append(StringUtil.NEWLINE)
        append(String(Base64.getEncoder().encode(encoded)))
        append(StringUtil.NEWLINE)
        append("-----END PUBLIC KEY-----")
    }

/**
 * Decrypts a shared key using this private key.
 *
 * The shared key must be encrypted with the private key's public key.
 *
 * @param data
 *      The shared key to decrypt.
 *
 * @return
 *      The shared key as decrypted with the private key.
 *      This is a [SecretKeySpec] using AES.
 */
fun PrivateKey.decryptSharedKey(data: ByteArray): SecretKeySpec = SecretKeySpec(decrypt(data), "AES")

/**
 * Encrypts the [data] to a [ByteArray] with this key.
 *
 * @param data
 *      The data to encrypt using the key.
 *
 * @return
 *      The encrypted version of [data].
 */
fun Key.encrypt(data: ByteArray): ByteArray = doCipherOperation(Cipher.ENCRYPT_MODE, this, data)

/**
 * Decrypts the [data] to a [ByteArray] with this key.
 *
 * This must have been encrypted with another key in the chain.
 *
 * @param data
 *      The data to decrypt using the key.
 *
 * @return
 *      The decrypted version of [data].
 */
fun PrivateKey.decrypt(data: ByteArray): ByteArray = doCipherOperation(Cipher.DECRYPT_MODE, this, data)

/**
 * Does either [Cipher.DECRYPT_MODE] or [Cipher.ENCRYPT_MODE] with the given [key] on the [data] and returns the output.
 *
 * @param mode
 *      The mode to use for the [Cipher].
 *
 * @param key
 *      The key to use for the action using the [Cipher].
 *      Decrypting requires this to be a [PrivateKey].
 *
 * @param data
 *      The data to use the [Cipher] with the [key] on and return the output of.
 *
 * @return
 *      The output after using a [Cipher] with [mode] using [key] on [data].
 */
fun doCipherOperation(mode: Int, key: Key, data: ByteArray): ByteArray = run {
    val cipher = Cipher.getInstance(key.algorithm)
    cipher.init(mode, key)

    cipher
}.doFinal(data)

/**
 * Creates an AES/CFB8/NoPadding cipher using the [key] and [mode] given.
 */
fun createCipher(mode: Int, key: Key): Cipher {
    val cipher = Cipher.getInstance("AES/CFB8/NoPadding")
    cipher.init(mode, key, IvParameterSpec(key.encoded))
    return cipher
}