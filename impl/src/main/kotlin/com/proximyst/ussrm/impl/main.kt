/*
 * Copyright (C) 2018 The USSRM Developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@file:JvmName("mainKt")

package com.proximyst.ussrm.impl

import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.joran.JoranConfigurator
import ch.qos.logback.core.joran.spi.JoranException
import ch.qos.logback.core.util.StatusPrinter
import com.proximyst.ussrm.api.Server
import com.proximyst.ussrm.api.log.logger
import org.slf4j.LoggerFactory
import java.lang.reflect.Field
import java.lang.reflect.Modifier

/**
 * Launches the server and sets the [Server.ServerInstanceHolder.server].
 */
fun main(args: Array<String>) {
    val loggerContext = LoggerFactory.getILoggerFactory() as LoggerContext
    try {
        val configurator = JoranConfigurator()
        configurator.context = loggerContext
        loggerContext.reset()
        configurator.doConfigure(Thread.currentThread().contextClassLoader.getResourceAsStream("logback.xml"))
    } catch (ignored: JoranException) {
        // StatusPrinter should handle this
    }
    StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext)

    SovietServer.logger.info("Server started. Overwriting Server.ServerInstanceHolder.[type Server]!")

    Server.ServerInstanceHolder::class.java.declaredFields
        .first {
            SovietServer.logger.debug("${it.name}: ${it.type.simpleName}")
            it.type == Server::class.java
        }
        .apply {
            SovietServer.logger.debug("Settled on $name: ${type.simpleName}")
            isAccessible = true
            SovietServer.logger.trace("Set isAccessible on $name")
            val modifiersField = Field::class.java.getDeclaredField("modifiers")
            modifiersField.isAccessible = true
            SovietServer.logger.trace("Set isAccessible on Field.modifiers")
            val oldModifiers = modifiers
            modifiersField.setInt(this, oldModifiers and Modifier.FINAL.inv())
            SovietServer.logger.trace("Changed modifiers from ${Integer.toBinaryString(oldModifiers)} to ${Integer.toBinaryString(modifiers)}")
            set(Server.ServerInstanceHolder, SovietServer)
        }

    SovietServer.logger.info("Overwritten; starting SovietServer!")
    SovietServer.start()
}